<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lead;
use Illuminate\Support\Facades\Mail;
use App\Mail\LeadSendMail;
use Illuminate\Support\Facades\Log;

class  LeadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $leads = Lead::All()->reverse();

        $data = [
            'title' => 'Заявки',
            'leads' => $leads,
        ];

        return view('admin.lead.index', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->except('_token');
        $lead = Lead::create($input);

        Log::channel('leads')->info('Заявка: '.$lead->tag.'; Телефон: '.$lead->phone.'; Создано: '.$lead->created_at);

        $emails = env('APP_ENV') == 'local' ? 'ojigov@inbox.ru' : ['spa@mosgym.ru','Isaeva-700@yandex.ru'];

        Mail::to($emails)->send(new LeadSendMail($lead));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Lead $lead)
    {

        $data = [
            'title' => 'Заявка',
            'lead' => $lead
        ];

        return view('admin.lead.show',$data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lead $lead)
    {
        $lead->delete();
        return redirect()->route('leads.index')->with('status','Заявка удалена');
    }
}

import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        siteUrl: 'https://www.instagram.com/mg_beauty_spa/',
        isModal: false,
        isSuccess: false,
    }
})
